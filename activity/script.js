console.log("Hello");
let num = 5;
let getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);

const address = ["258 Washington Ave NW", "California", "90011"];
const [street, state, zipCode] = address;

console.log (`I live at ${street}, ${state} ${zipCode}.`);

const animal = {
	name: "Lolong",
	type: "salt water crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in",
}

const {name, type, weight, measurement} = animal

console.log(`${name} was a ${type}. He weighed at ${weight} with measurement of ${measurement}.`)

 let numbers = [1, 2, 3, 4, 5]

 numbers.forEach((numbers) =>console.log(numbers));

 let reducedNumber = numbers.reduce((x, y) => x + y);
 console.log(reducedNumber)


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed
	}
}

	const myDog = new Dog('Frankie', 5, "Miniature Dachshud")
	console.log(myDog);
	